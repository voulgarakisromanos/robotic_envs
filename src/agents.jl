using ReinforcementLearning
using Flux

create_policy_net() = NeuralNetworkApproximator(
    model=GaussianNetwork(
        pre=Chain(
            Dense(ns, 256, relu, init=init),
            Dense(256, 256, relu, init=init),
            Dense(256, 256, relu, init=init),
            Dense(256, 256, relu, init=init)
        ),
        μ=Chain(Dense(256, na, init=init)),
        logσ=Chain(Dense(256, na, init=init)),
    ),
    optimizer=ADAM(0.003),
) |> gpu

create_q_net() = NeuralNetworkApproximator(
model=Chain(
    Dense(ns + na, 256, relu; init=init),
    Dense(256, 256, relu; init=init),
    Dense(256, 256, relu, init=init),
    Dense(256, 256, relu, init=init),
    Dense(256, 1; init=init),
),
optimizer=ADAM(0.003),
) |> gpu

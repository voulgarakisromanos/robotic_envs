using PythonCall
using ReinforcementLearning
using BSON
using Dates

keyboard_module = pyimport("robosuite.devices.keyboard")
input_utils = pyimport("robosuite.utils.input_utils")

function collect_multiple_trajectories(env; n_epochs=5)
    na, ns = size(action_space(env))[1], size(state_space(env))[1]

    trajectory=CircularArraySARTTrajectory(
        capacity=1_000_000,
        state=Vector{Float32} => (ns,),
        action=Vector{Float32} => (na,),
    )
    for i=1:n_epochs
        collect_trajectories!(env, trajectory)
    end
    return trajectory
end


function collect_trajectories!(env, trajectory)
    keyboard_device = keyboard_module.Keyboard(pos_sensitivity=1, rot_sensitivity=1)
    return collect_trajectories!(env,keyboard_device, trajectory)
end

function collect_trajectories!(env, device, trajectory)

    reset!(env)

    show(env)

    task_completion_hold_count = -1  # counter to collect 10 timesteps after reaching goal
    device.start_control()

    # Loop until we get a reset from the input or the task completes
    while true
        # Set active robot
        active_robot = env.ptr.robots[0]
        # Get the newest action
        action, grasp = input_utils.input2action(device=device, robot=active_robot)
        try
            action = pyconvert(Nothing,action)
            grasp = pyconvert(Nothing, grasp)
        catch 
        end
  
        # If action is none, then this a reset so we should break
        if isnothing(action)
            break
        end

        # Run environment step
        state_before = state(env)
        println(action)
        env(action)

        # if pyconvert(Vector,action) != [0.0, 0.0, 0.0, -1.0]
        push!(trajectory, state=state_before, action=pyconvert(Vector{Float32}, action), reward=env.reward, terminal=env.done)
        # end
        
        show(env)

        # Also break if we complete the task
        if task_completion_hold_count == 0
            break
        end

        # state machine to check for having a success for 10 consecutive timesteps
        if pyconvert(Bool,env.ptr._check_success().tolist())
            if task_completion_hold_count > 0
                task_completion_hold_count -= 1  # latched state, decrement count
            else
                task_completion_hold_count = 10  # reset count on first success timestep
            end
        else
            task_completion_hold_count = -1  # null the counter if there's no success
        end
    end
    return trajectory
    # BSON.@save string(now(),".bson") trajectory
    # cleanup for end of data collection episodes
end


function combine_named_tuples(tuple1, tuple2)
    @assert keys(tuple1)==keys(tuple2)
    tuple_keys = keys(tuple1)
    combined_tuple = NamedTuple(key => (typeof(tuple1[key]) <: Vector ? vcat(tuple1[key],tuple2[key]) : hcat(tuple1[key],tuple2[key])) for key in tuple_keys)
end